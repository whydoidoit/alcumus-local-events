const HookedEvents = require('./hooked-events')

let start = Date.now()

if(global.__LOCAL_EVENTS && global.__LOCAL_EVENTS !== start) {
    console.error("LOCAL EVENTS LOADED MORE THAN ONCE")
}

global.__LOCAL_EVENTS = start

module.exports = new HookedEvents()

module.exports.events = module.exports
