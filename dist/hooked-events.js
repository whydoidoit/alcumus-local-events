"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Events = require('eventemitter2');

var _emit = Events.prototype.emit;
var _emitAsync = Events.prototype.emitAsync;

var HookedEvents =
/*#__PURE__*/
function (_Events) {
  _inherits(HookedEvents, _Events);

  _createClass(HookedEvents, [{
    key: "emitAsync",
    value: function () {
      var _emitAsync2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var context,
            delim,
            _len,
            parms,
            _key,
            eventName,
            i,
            _i,
            _i2,
            _i3,
            _args = arguments;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                context = {
                  cancel: false,
                  preventDefault: function preventDefault() {
                    context.cancel = true;
                  }
                };
                delim = this.delimiter || ':';

                for (_len = _args.length, parms = new Array(_len), _key = 0; _key < _len; _key++) {
                  parms[_key] = _args[_key];
                }

                eventName = parms[0];
                parms.splice(1, 0, context);

                if (!this.all) {
                  _context.next = 16;
                  break;
                }

                i = 0;

              case 7:
                if (!(i < 2)) {
                  _context.next = 16;
                  break;
                }

                parms[0] = "early".concat(delim).concat(i).concat(delim).concat(eventName);
                _context.next = 11;
                return _emitAsync.apply(this, parms);

              case 11:
                if (!context.cancel) {
                  _context.next = 13;
                  break;
                }

                return _context.abrupt("return", false);

              case 13:
                i++;
                _context.next = 7;
                break;

              case 16:
                parms[0] = "before".concat(delim).concat(eventName);
                _context.next = 19;
                return _emitAsync.apply(this, parms);

              case 19:
                if (!context.cancel) {
                  _context.next = 21;
                  break;
                }

                return _context.abrupt("return", false);

              case 21:
                if (!this.all) {
                  _context.next = 32;
                  break;
                }

                _i = 0;

              case 23:
                if (!(_i < 2)) {
                  _context.next = 32;
                  break;
                }

                parms[0] = "pre".concat(delim).concat(_i).concat(delim).concat(eventName);
                _context.next = 27;
                return _emitAsync.apply(this, parms);

              case 27:
                if (!context.cancel) {
                  _context.next = 29;
                  break;
                }

                return _context.abrupt("return", false);

              case 29:
                _i++;
                _context.next = 23;
                break;

              case 32:
                parms[0] = eventName;
                _context.next = 35;
                return _emitAsync.apply(this, parms);

              case 35:
                if (!context.cancel) {
                  _context.next = 37;
                  break;
                }

                return _context.abrupt("return", false);

              case 37:
                if (!this.all) {
                  _context.next = 48;
                  break;
                }

                _i2 = 0;

              case 39:
                if (!(_i2 < 2)) {
                  _context.next = 48;
                  break;
                }

                parms[0] = "post".concat(delim).concat(_i2).concat(delim).concat(eventName);
                _context.next = 43;
                return _emitAsync.apply(this, parms);

              case 43:
                if (!context.cancel) {
                  _context.next = 45;
                  break;
                }

                return _context.abrupt("return", false);

              case 45:
                _i2++;
                _context.next = 39;
                break;

              case 48:
                parms[0] = "after".concat(delim).concat(eventName);
                _context.next = 51;
                return _emitAsync.apply(this, parms);

              case 51:
                if (!context.cancel) {
                  _context.next = 53;
                  break;
                }

                return _context.abrupt("return", false);

              case 53:
                if (!this.all) {
                  _context.next = 64;
                  break;
                }

                _i3 = 0;

              case 55:
                if (!(_i3 < 2)) {
                  _context.next = 64;
                  break;
                }

                parms[0] = "late".concat(delim).concat(_i3).concat(delim).concat(eventName);
                _context.next = 59;
                return _emitAsync.apply(this, parms);

              case 59:
                if (!context.cancel) {
                  _context.next = 61;
                  break;
                }

                return _context.abrupt("return", false);

              case 61:
                _i3++;
                _context.next = 55;
                break;

              case 64:
                return _context.abrupt("return", true);

              case 65:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function emitAsync() {
        return _emitAsync2.apply(this, arguments);
      }

      return emitAsync;
    }()
  }, {
    key: "emit",
    value: function emit() {
      var context = {
        cancel: false,
        preventDefault: function preventDefault() {
          context.cancel = true;
        }
      };
      var delim = this.delimiter || ':';

      for (var _len2 = arguments.length, parms = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        parms[_key2] = arguments[_key2];
      }

      var eventName = parms[0];
      parms.splice(1, 0, context);

      if (this.all) {
        for (var i = 0; i < 2; i++) {
          parms[0] = "early".concat(delim).concat(i).concat(delim).concat(eventName);

          _emit.apply(this, parms);

          if (context.cancel) return false;
        }
      }

      parms[0] = "before".concat(delim).concat(eventName);

      _emit.apply(this, parms);

      if (context.cancel) return false;

      if (this.all) {
        for (var _i4 = 0; _i4 < 2; _i4++) {
          parms[0] = "pre".concat(delim).concat(_i4).concat(delim).concat(eventName);

          _emit.apply(this, parms);

          if (context.cancel) return false;
        }
      }

      parms[0] = eventName;

      _emit.apply(this, parms);

      if (context.cancel) return false;

      if (this.all) {
        for (var _i5 = 0; _i5 < 2; _i5++) {
          parms[0] = "post".concat(delim).concat(_i5).concat(delim).concat(eventName);

          _emit.apply(this, parms);

          if (context.cancel) return false;
        }
      }

      parms[0] = "after".concat(delim).concat(eventName);

      _emit.apply(this, parms);

      if (context.cancel) return false;

      if (this.all) {
        for (var _i6 = 0; _i6 < 2; _i6++) {
          parms[0] = "late".concat(delim).concat(_i6).concat(delim).concat(eventName);

          _emit.apply(this, parms);

          if (context.cancel) return false;
        }
      }

      return true;
    }
  }, {
    key: "bindEvent",
    value: function bindEvent(event, async) {
      return function () {
        for (var _len3 = arguments.length, parameters = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          parameters[_key3] = arguments[_key3];
        }

        if (async) {
          _emitAsync.apply(void 0, [event].concat(parameters));
        } else {
          _emit.apply(void 0, [event].concat(parameters));
        }
      };
    }
  }, {
    key: "return",
    value: function _return(event, fn) {
      var _this = this;

      if (!fn) {
        return function (fn) {
          if (typeof fn !== 'function') {
            var v = fn;

            fn = function fn() {
              return v;
            };
          }

          _this.on(event, eventUsingParameter(fn));
        };
      }

      this.on(event, eventUsingParameter(fn));
    }
  }, {
    key: "returnAsync",
    value: function returnAsync(event, fn) {
      var _this2 = this;

      if (!fn) {
        return function (fn) {
          if (typeof fn !== 'function') {
            var v = fn;

            fn = function fn() {
              return Promise.resolve(v);
            };
          }

          _this2.on(event, asyncEventUsingParameter(fn));
        };
      }

      this.on(event, asyncEventUsingParameter(fn));
    }
  }, {
    key: "modify",
    value: function modify(event, _modify) {
      var _this3 = this;

      for (var _len4 = arguments.length, params = new Array(_len4 > 2 ? _len4 - 2 : 0), _key4 = 2; _key4 < _len4; _key4++) {
        params[_key4 - 2] = arguments[_key4];
      }

      if (!_modify) {
        return function (modify) {
          return modifyValueUsingEvent.apply(void 0, [event, modify, _this3].concat(params));
        };
      }

      return modifyValueUsingEvent.apply(void 0, [event, _modify, this].concat(params));
    }
  }, {
    key: "modifyAsync",
    value: function () {
      var _modifyAsync = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(event, modify) {
        var _this4 = this;

        var _len5,
            params,
            _key5,
            _args3 = arguments;

        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                for (_len5 = _args3.length, params = new Array(_len5 > 2 ? _len5 - 2 : 0), _key5 = 2; _key5 < _len5; _key5++) {
                  params[_key5 - 2] = _args3[_key5];
                }

                if (modify) {
                  _context3.next = 3;
                  break;
                }

                return _context3.abrupt("return",
                /*#__PURE__*/
                function () {
                  var _ref = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee2(modify) {
                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            _context2.next = 2;
                            return modifyValueUsingEventAsync.apply(void 0, [event, modify, _this4].concat(params));

                          case 2:
                            return _context2.abrupt("return", _context2.sent);

                          case 3:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2, this);
                  }));

                  return function (_x3) {
                    return _ref.apply(this, arguments);
                  };
                }());

              case 3:
                _context3.next = 5;
                return modifyValueUsingEventAsync.apply(void 0, [event, modify, this].concat(params));

              case 5:
                return _context3.abrupt("return", _context3.sent);

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function modifyAsync(_x, _x2) {
        return _modifyAsync.apply(this, arguments);
      }

      return modifyAsync;
    }()
  }]);

  function HookedEvents(props) {
    _classCallCheck(this, HookedEvents);

    return _possibleConstructorReturn(this, _getPrototypeOf(HookedEvents).call(this, props));
  }

  _createClass(HookedEvents, [{
    key: "onAll",
    value: function onAll(events, fn) {
      if (typeof events === 'string') events = events.split(' ').map(function (e) {
        return e.trim();
      }).filter(function (f) {
        return !!f;
      });
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = events[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var type = _step.value;
          this.on(type, fn);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "offAll",
    value: function offAll(events, fn) {
      if (typeof events === 'string') events = events.split(' ').map(function (e) {
        return e.trim();
      }).filter(function (f) {
        return !!f;
      });
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = events[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var type = _step2.value;
          this.off(type, fn);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    }
  }, {
    key: "use",
    value: function use(handler) {
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = methods(handler)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var _step3$value = _slicedToArray(_step3.value, 2),
              event = _step3$value[0],
              fn = _step3$value[1];

          this.on(clean(event), fn);
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }
    }
  }, {
    key: "discard",
    value: function discard(handler) {
      var _iteratorNormalCompletion4 = true;
      var _didIteratorError4 = false;
      var _iteratorError4 = undefined;

      try {
        for (var _iterator4 = methods(handler)[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
          var _step4$value = _slicedToArray(_step4.value, 2),
              event = _step4$value[0],
              fn = _step4$value[1];

          this.off(clean(event), fn);
        }
      } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
            _iterator4.return();
          }
        } finally {
          if (_didIteratorError4) {
            throw _iteratorError4;
          }
        }
      }
    }
  }]);

  return HookedEvents;
}(Events);

function clean(name) {
  return name.replace(/_/g, '.').replace(/\$/g, '*');
}

function methods(klass) {
  var properties = [];
  var _iteratorNormalCompletion5 = true;
  var _didIteratorError5 = false;
  var _iteratorError5 = undefined;

  try {
    for (var _iterator5 = Object.getOwnPropertyNames(klass)[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
      var item = _step5.value;

      if (typeof klass[item] === 'function') {
        properties.push([item, klass[item]]);
      }
    }
  } catch (err) {
    _didIteratorError5 = true;
    _iteratorError5 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
        _iterator5.return();
      }
    } finally {
      if (_didIteratorError5) {
        throw _iteratorError5;
      }
    }
  }

  return properties;
}

function processResult(fn, resultObject, result) {
  if (fn.length >= 1) {
    resultObject.parameters = result || resultObject.parameters;
  } else {
    if (result) {
      if (Array.isArray(resultObject.parameters)) {
        if (Array.isArray(result)) {
          resultObject.parameters.push.apply(resultObject.parameters, result);
        } else {
          resultObject.parameters.push(result);
        }
      } else if (_typeof(resultObject.parameters) === 'object') {
        Object.assign(resultObject.parameters, result);
      } else {
        resultObject.parameters = result || resultObject.parameters;
      }
    }
  }

  resultObject.updated = (resultObject.updated || 0) + 1;
}

function eventUsingParameter(fn) {
  return function (event, resultObject) {
    var result = fn(resultObject.parameters, resultObject.updated);
    processResult(fn, resultObject, result);
  };
}

function asyncEventUsingParameter(fn) {
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(event, resultObject) {
        var result;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return fn(resultObject.parameters, resultObject.updated);

              case 2:
                result = _context4.sent;
                processResult(fn, resultObject, result);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      return function (_x4, _x5) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
}

function modifyValueUsingEvent(event, parameters, eventEmitter) {
  var resultObject = {
    parameters: parameters
  };

  for (var _len6 = arguments.length, params = new Array(_len6 > 3 ? _len6 - 3 : 0), _key6 = 3; _key6 < _len6; _key6++) {
    params[_key6 - 3] = arguments[_key6];
  }

  eventEmitter.emit.apply(eventEmitter, [event, resultObject].concat(params));
  return resultObject.parameters;
}

function modifyValueUsingEventAsync(_x6, _x7, _x8) {
  return _modifyValueUsingEventAsync.apply(this, arguments);
}

function _modifyValueUsingEventAsync() {
  _modifyValueUsingEventAsync = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(event, parameters, eventEmitter) {
    var resultObject,
        _len7,
        params,
        _key7,
        _args5 = arguments;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            resultObject = {
              parameters: parameters
            };

            for (_len7 = _args5.length, params = new Array(_len7 > 3 ? _len7 - 3 : 0), _key7 = 3; _key7 < _len7; _key7++) {
              params[_key7 - 3] = _args5[_key7];
            }

            _context5.next = 4;
            return eventEmitter.emitAsync.apply(eventEmitter, [event, resultObject].concat(params));

          case 4:
            return _context5.abrupt("return", resultObject.parameters);

          case 5:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, this);
  }));
  return _modifyValueUsingEventAsync.apply(this, arguments);
}

module.exports = HookedEvents;