const events = require('../index')
const expect = require('chai').expect

describe("Local Events", function () {
    beforeEach(function() {
        events.all = true
    })
    afterEach(function () {
        events.removeAllListeners()
    })
    describe("Synchronous", function () {

        it("should fire a local event", function (done) {
            console.log("A")
            events.on('test', () => done())
            console.log("B")

            events.emit('test')
            console.log("C")
        })
        it("should trap a wildcard event", function (done) {
            events.on('test.*', () => done())
            events.emit('test.one')
        })
        it("should trap a pre event", function (done) {
            events.on('pre.1.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should trap a post event", function (done) {
            events.on('post.1.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should trap a before event", function (done) {
            events.on('before.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should trap an after event", function (done) {
            events.on('after.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should trap a late event", function (done) {
            events.on('late.1.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should trap an early event", function (done) {
            events.on('early.1.test', function () {
                done()
            })
            events.emit('test')
        })
        it("should fire events in order", function () {
            let counter = 0
            events.on('early.1.test', function () {
                expect(counter++).to.equal(0)
            })
            events.on('before.test', function () {
                expect(counter++).to.equal(1)
            })
            events.on('pre.1.test', function () {
                expect(counter++).to.equal(2)
            })
            events.on('test', function () {
                expect(counter++).to.equal(3)
            })
            events.on('post.1.test', function () {
                expect(counter++).to.equal(4)
            })
            events.on('after.test', function () {
                expect(counter++).to.equal(5)
            })
            events.on('late.1.test', function () {
                expect(counter++).to.equal(6)
            })
            events.emit('test')
            expect(counter).to.equal(7)
        })
        it("should enable an event to be cancelled", function() {
            let counter = 0
            events.on('test', function() {
                throw new Error("Should not be here")
            })
            events.on('before.test', function(context) {
                context.preventDefault()
                counter++
            })
            events.emit('test')
            expect(counter).to.equal(1)
        })
        it("should take multiple event parameters", function() {
            let counter = 0
            events.on('test', function(context, a, b) {
                expect(a).to.equal('a')
                expect(b).to.equal('b')
                expect(context.cancel).to.equal(false)
                counter++
            })
            events.emit('test', 'a', 'b')
            expect(counter).to.equal(1)
        })
    })
    describe("Asynchronous", function () {
        function sleep(time) {
            return new Promise(function(resolve) {
                setTimeout(resolve, time * 1000)
            })
        }
        it("should fire events in order", async function () {
            let counter = 0
            events.on('early.1.test', function () {
                expect(counter++).to.equal(0)
            })
            events.on('before.test', function () {
                expect(counter++).to.equal(1)
            })
            events.on('pre.1.test', function () {
                expect(counter++).to.equal(2)
            })
            events.on('test', function () {
                expect(counter++).to.equal(3)
            })
            events.on('post.1.test', function () {
                expect(counter++).to.equal(4)
            })
            events.on('after.test', function () {
                expect(counter++).to.equal(5)
            })
            events.on('late.1.test', function () {
                expect(counter++).to.equal(6)
            })
            await events.emitAsync('test')
            expect(counter).to.equal(7)
        })
        it("should enable an event to be cancelled", async function () {
            let counter = 0
            events.on('test', function () {
                throw new Error("Should not be here")
            })
            events.on('before.test', async function (context) {
                await sleep(1)
                context.preventDefault()
                counter++
            })
            await events.emitAsync('test')
            expect(counter).to.equal(1)
        })
        it('should fire on any of the events', async function () {
            let counter = 0
            events.onAll('test test2', ()=>counter++)
            events.emit('test')
            events.emit('test2')
            expect(counter).to.equal(2)
        })
        it('should wire an object', function() {
            let counter = 0
            events.use({
                'test.1'() {counter++},
                'test.2'() {counter++},
                'test.*'() {counter++}
            })
            events.emit('test.1')
            events.emit('test.2')
            expect(counter).to.equal(4)
        })
        it('should wire a class', function() {
            let counter = 0
            events.use(class Handler {
                static 'test.1'() { counter ++}
                static 'test.2'() { counter ++}
                static 'test.*'() { counter ++}
            })
            events.emit('test.1')
            events.emit('test.2')
            expect(counter).to.equal(4)
        })
        it('should map to dot', function () {
            let counter = 0
            events.use(class Handler {
                static test_1() { counter ++}
                static test_2() { counter ++}
                static test_$() { counter ++}
                static test_$$() { counter ++}

            })
            events.emit('test.1')
            events.emit('test.2')
            events.emit('test.2.3')
            expect(counter).to.equal(7)
        })

    })
})
