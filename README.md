# Local Events & Hookable Event Emitter

The local events module provides a project wide method of creating loosely coupled events that have the ability to provide hooks to override default behaviour or to apply additional processing before or after standard events.

The library provides a standard local-events emitter and an upgraded version of [eventemitter2](https://github.com/EventEmitter2/EventEmitter2) that can be used to create other hookable events.

## Hookable Events

All of the events fired have multiple steps that can be used to override or augment default behaviour.

Events are fired with additional elements prepended to allow for a variety of hooks.  The elements are divided by the standard delimiter, which is '.' as standard.  The order of events for 'EVENT' is `early.0.EVENT` - `early.9.EVENT`, `before.EVENT`, `pre.0.EVENT` - `pre.9.EVENT`, `EVENT`, `post.0.EVENT` - `post.9.EVENT`, `after.EVENT`, `late.0.EVENT` - `late.9.EVENT`

Each event processing function is passed a first parameter to indicate *context*, this has a `cancel` property and a `preventDefault()` method which causes the chain of events to be aborted.


```javascript
    let counter = 0
    events.on('test', function() {
        throw new Error("Should not be here")
    })
    events.on('pre.4.test', function() {
        counter++
    })
    events.on('before.test', function(context) {
        context.preventDefault()
        counter++
    })
    events.emit('test')
    expect(counter).to.equal(2)
```

You may also use the context object to pass other data between event invocations.

### Wildcard Events

The eventemitter2 implementation allows multipart events with wildcard support.  For example:

```javascript

    events.on('registerUser.*.start', function(context, param1, param2) {
        console.log("Generic user start")
    })
    
    events.on('before.registerUser.tfl.start', async function(context, param1, param2) {
        console.log("Hook tfl before Generic user start")
        if(param1 === 'admin') {
            if(!await callSomething(param2)) {
                context.preventDefault()
            }
        }
    })

    await events.emitAsync(`registerUser.${clientName}.start`, role, authToken)
```

### Asynchronous Events

eventemitter2 fully supports asynchronous events using `emitAsync`.

```javascript
    async function test() {
        let counter = 0
        events.on('early.1.test', function () {
            expect(counter++).to.equal(0)
        })
        events.on('before.test', function () {
            expect(counter++).to.equal(1)
        })
        events.on('pre.1.test', function () {
            expect(counter++).to.equal(2)
        })
        events.on('test', function () {
            expect(counter++).to.equal(3)
        })
        events.on('post.1.test', function () {
            expect(counter++).to.equal(4)
        })
        events.on('after.test', function () {
            expect(counter++).to.equal(5)
        })
        events.on('late.1.test', function () {
            expect(counter++).to.equal(6)
        })
        await events.emitAsync('test')
        expect(counter).to.equal(7)
    }
```
### Usage

```javascript
const HookedEvents = require('local-events/hooked-events')
const myEmitter = new HookedEvents({wildcard: true, delimiter: '::'})
```

## Local Events

The local-events object is a configured hooked eventemitter2 instance with wildcards supported and the delimiter set to be '.' (it also supports up to 200 listeners per event before warning).

local-events can therefore be used across an entire project to react to and raise events.

### Usage

`const events = require('local-events')`
